import { ui } from "./../ui/layaMaxUI";
import AStarFinder from "./AStar/AStarFinder";
import AStarJson from "./AStar/AStarJson";
import AStarMap from "./AStar/AStarMap";
import cubectrl from "./cubectrl";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    private newScene:Laya.Scene3D;
    constructor() {
        super();
		
		// this.newScene = Laya.stage.addChild(new Laya.Scene3D()) as Laya.Scene3D;
		
		Laya.loader.create("res/scene/Conventional/test.ls", Laya.Handler.create(this, (res) => { 
			this.newScene = res;
			Laya.stage.addChild(this.newScene);

			this.newScene.getChildByName('Cube').addComponent(cubectrl);
			AStarFinder.GetInstance().SetAStarMap(JSON.parse(AStarJson.json ) as AStarMap);
			window['xx'] = this.startAgent();
		}))
    }

	public startAgent() {
		let cc = this.newScene.getChildByName('Cube').getComponent(cubectrl) as cubectrl;
		let sp = (this.newScene.getChildByName('Sphere') as Laya.Sprite3D).transform.position;
		cc.Agent(sp);
	}
}