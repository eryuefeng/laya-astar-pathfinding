/**
* @ name:SingtonBase
* @ desc:单例基类 方便继承 后续建议执行拓展 
* @ user:By NUOLO
* @ data: 2020-10-30 19:51
*/
export default class SingtonBase{
   public static GetInstance<T extends {}>(this: new () => T): T {
        if(!(<any>this).instance){
            (<any>this).instance = new this();
        }
        return (<any>this).instance;
    }

}