export default class AStarMap
{
    public  width = 0;
    public  height = 0;
    public  minx = 0;
    public  minz = 0;
    public  cellX = 1;
    public  cellZ = 1;
    //0代表可走，1代表不可走
    public  matrix = [];

    public poslist = [];

}