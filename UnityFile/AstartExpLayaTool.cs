using System;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class AStarMap
{
    public int width = 0;
    public int height = 0;
    public int minx = 0;
    public int minz = 0;
    public int cellX = 1;
    public int cellZ = 1;
    //0代表可走，1代表不可走
    public List<int[]> matrix = new List<int[]>();

    public List<NewInt3[]> poslist = new List<NewInt3[]>();

}

public  class NewInt3
{
    public int x, y, z;
    public NewInt3(int tx,int ty,int tz)
    {
        x = tx;
        y = ty;z = tz;
    }
}


public class AstartExpLayaTool : MonoBehaviour
{

    public List<Vector3> listpos;
    public List<bool> boollist;

   // 导航烘焙的总点数
   [Range(0, 11128)] public int Amout = 0;
    public AStarMap _AStarMap;

    [ContextMenu("exp json")]
    void expjson()
    {
        listpos = new List<Vector3>();
        boollist = new List<bool>();
        var astartMap=this.GetComponent<AstarPath>();

        var u3dMap=astartMap.graphs[0] as Pathfinding.GridGraph;

        var layamap = new AStarMap();
        layamap.width = u3dMap.width;
        layamap.height = u3dMap.depth;

        var nodeIdx = 0;
        for (int i = 0; i < u3dMap.depth; i++)
        {
            var line = new int[u3dMap.width];
            var linevec = new NewInt3[u3dMap.width];
            for (int j = 0; j < u3dMap.width; j++)
            {
                 line[j] = u3dMap.nodes[nodeIdx].Walkable ?1 : 0;
                linevec[j] = new NewInt3( -u3dMap.nodes[nodeIdx].position.x, u3dMap.nodes[nodeIdx].position.y, u3dMap.nodes[nodeIdx].position.z);
                listpos.Add(new Vector3(u3dMap.nodes[nodeIdx].position.x, u3dMap.nodes[nodeIdx].position.y, u3dMap.nodes[nodeIdx].position.z));
                boollist.Add( u3dMap.nodes[nodeIdx].Walkable);
                nodeIdx += 1;
            }

            layamap.matrix.Add(line);
            layamap.poslist.Add(linevec);
        }

        //_AStarMap = layamap;

       // Debug.Log(Newtonsoft.Json.Serialization.json layamap);
        string jsondata = JsonConvert.SerializeObject(layamap);
        Debug.Log("数据已拷贝到黏贴板");
        Debug.Log(jsondata);
        UnityEngine.GUIUtility.systemCopyBuffer = jsondata;

    }


    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;//为随后绘制的gizmos设置颜色。
        if (listpos.Count!=0)
        {
            for (var i = 0; i < Amout; i++)
            {
                if (boollist[i])
                { //可以通过
                    Gizmos.color = Color.blue;//为随后绘制的gizmos设置颜色。
                }
                else
                {//不可通过
                    Gizmos.color = Color.red;//为随后绘制的gizmos设置颜色。
                }
                Gizmos.DrawWireSphere(listpos[i]/1000,.25f);//使用center和radius参数，绘制一个线框球体。
            }
          
        }
        
    }
}

