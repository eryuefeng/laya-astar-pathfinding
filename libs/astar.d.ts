declare class astar {
      /**
         * Perform an A* Search on a graph given a start and end node.
         * @param {Graph} graph
         * @param {GridNode} start
         * @param {GridNode} end
         * @param {Object} [options]
         * @param {bool} [options.closest] Specifies whether to return the
                     path to the closest node if the target is unreachable.
        * @param {Function} [options.heuristic] Heuristic function (see
        *          astar.heuristics).
        */
    static search(graph, start, end, options?);
    cleanNode(node);
}

declare class Graph {
    /**
     * A graph memory structure
     * @param {Array} gridIn 2D array of input weights
     * @param {Object} [options]
     * @param {bool} [options.diagonal] Specifies whether diagonal moves are allowed
     */
    constructor(gridIn, options?)
    grid: any;
   
}

declare class GridNode {
    x;
    y;
    weight;
    constructor(x?, y?, weight?)
}